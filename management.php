<?php
  session_start();

  if (!isset($_SESSION['id_sesion']))
  {
    header("Location:index.html");
  }else{
?>
<!DOCTYPE html>
<html lang="en">
<?php
  require "php/styles.php"; //llamar hojas de estilos
  require "php/enviar_correo.php";//llamar FUNCIONES PARA ENVIO DE CORREO
?>

  <body>
    <?php

      require "php/adm_pag.php";//llama al administrador de paginas

      pag('nav-bar'); //llamar barra navegacion superior
    ?>

    <div class="container-fluid"><!-- INICIO DEL CONTENEDOR GENERAL-->        
        <div class="col-xs-12 col-sm-12 col-md-12"><!-- CONTENEDOR PARA CONTENIDO PRINCIPAL -->
              <section class="row">
              <?php

                if (isset($_REQUEST['pag'])) {
                  pag($_REQUEST['pag']); //llama la pagina seleccionada
                }else{
                  pag('inicio'); // si no hay seleccionada ninguna pagina, carga pagina inicio
                }

                pag('footer'); //llamar funcion de footer
              ?>
              </section>
              
        </div>
    </div><!-- FIN DE CONTENEDOR GENERAL -->

        <script type="text/javascript" src="bootstrap/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/styles.js"></script>
  </body>
</html>
<?php
}
?>