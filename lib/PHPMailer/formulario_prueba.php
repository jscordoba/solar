<html>
<head>
<title>Formato de solicitud de cotizaci&oacute;n</title>
<link href="tablecloth/tablecloth.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="tablecloth/tablecloth.js"></script>
<script type="text/javascript" src="js/paises.js"></script>
</head>
<body>
<?php
include_once('conexion.php');

?>
<FORM METHOD="post" ACTION="formulario_prueba.php">
<table border='3'>
	<tr>
		<td colspan='4'><h5>C&oacute;digo / Code : F-PV2-13</h5></td>
	</tr>
	<tr>
		<td><IMG SRC="magnetron.JPG" WIDTH=150 HEIGHT=150 ALT="Logo"></td>
		<td align='center'><h1>MAGNETRON S.A.S.</h1></td>
		<td><h4>DEPARTAMENTO DE VENTAS / SALES DEPARTMENT</h4></td>
		<td align='right'><h1>FORMATO SOLICITUD DE COTIZACION / QOUTATION REQUIREMENT</h1></td>
	</tr>
	<tr>
		<td align="center" colspan="4"><font color="red">NOTA: Los campos con (*) son obligatorios / Fields with (*) are mandatory</font></td>
	</tr>
	<tr>
			<td align="center" colspan='4'>

			<table border= '1'>
				<tr>
					<td align="center" colspan='5'>
					<b>1. INFORMACION GENERAL DE CLIENTE / CUSTOMER INFORMATION</b>
					</td>
				
				</tr>
				<tr>
					<td>Empresa / Company <font color="red">*</font></td>
					<td> <input type="text"  size=300 style="width:300px" name="empresa" value="<?php echo $_POST['empresa']; ?>"></td>
					
					<td>Contacto / Contact <font color="red">*</font></td>
					<td colspan="2"> <input type="text"  size=300 style="width:300px" name="contacto" value="<?php echo $_POST['contacto']; ?>"></td>
				</tr>
				<tr>
					<td>Pa&iacute;s / Country <font color="red">*</font></td>
					<td><select name="pais" size="1" id="pais" onChange="Mostrar(this)">
						<!--<option value="" selected="selected"></option>-->
						<option value=""></option>
						<option value="Afganistan">Afganistan</option>
						<option value="Albania">Albania</option>
						<option value="Alemania">Alemania</option>
						<option value="Andorra">Andorra</option>
						<option value="Angola">Angola</option>
						<option value="Anguilla">Anguilla</option>
						<option value="Antartida">Antartida</option>
						<option value="Antigua y Barbuda">Antigua y Barbuda</option>
						<option value="Antillas Holandesas">Antillas Holandesas</option>
						<option value="Arabia Saud">Arabia Saud</option>
						<option value="Argelia">Argelia</option>
						<option value="Argentina">Argentina</option>
						<option value="Armenia">Armenia</option>
						<option value="Aruba">Aruba</option>
						<option value="Australia">Australia</option>
						<option value="Austria">Austria</option>
						<option value="Azerbaiy�n">Azerbaiy�n</option>
						<option value="Bahamas">Bahamas</option>
						<option value="Bahrein">Bahrein</option>
						<option value="Bangladesh">Bangladesh</option>
						<option value="Barbados">Barbados</option>
						<option value="Belgica">Belgica</option>
						<option value="Belice">Belice</option>
						<option value="Benin">Benin</option>
						<option value="Bermudas">Bermudas</option>
						<option value="Bielorrusia">Bielorrusia</option>
						<option value="Birmania">Birmania</option>
						<option value="Bolivia">Bolivia</option>
						<option value="Bosnia y Herzegovina">Bosnia y Herzegovina</option>
						<option value="Botswana">Botswana</option>
						<option value="Brasil">Brasil</option>
						<option value="Brunei">Brunei</option>
						<option value="Bulgaria">Bulgaria</option>
						<option value="Burkina Faso">Burkina Faso</option>
						<option value="Burundi">Burundi</option>
						<option value="But�n">But�n</option>
						<option value="Cabo Verde">Cabo Verde</option>
						<option value="Camboya">Camboya</option>
						<option value="Camer�n">Camer�n</option>
						<option value="Canada">Canada</option>
						<option value="Chad">Chad</option>
						<option value="Chile">Chile</option>
						<option value="China">China</option>
						<option value="Chipre">Chipre</option>
						<option value="Ciudad del Vaticano">Ciudad del Vaticano</option>
						<option value="Colombia">Colombia</option>
						<option value="Comores">Comores</option>
						<option value="Congo">Congo</option>
						<option value="Congo RD">Congo RD</option>
						<option value="Corea">Corea</option>
						<option value="Corea del Norte">Corea del Norte</option>
						<option value="Costa de Marfil">Costa de Marfil</option>
						<option value="Costa Rica">Costa Rica</option>
						<option value="Croacia (Hrvatska)">Croacia (Hrvatska)</option>
						<option value="Cuba">Cuba</option>
						<option value="Dinamarca">Dinamarca</option>
						<option value="Djibouti">Djibouti</option>
						<option value="Dominica">Dominica</option>
						<option value="Ecuador">Ecuador</option>
						<option value="Egipto">Egipto</option>
						<option value="El Salvador">El Salvador</option>
						<option value="Emiratos �rabes Unidos">Emiratos �rabes Unidos</option>
						<option value="Eritrea">Eritrea</option>
						<option value="Eslovenia">Eslovenia</option>
						<option value="Espa�a">Espa�a</option>
						<option value="Estados Unidos">Estados Unidos</option>
						<option value="Estonia">Estonia</option>
						<option value="Etiopia">Etiopia</option>
						<option value="Fiji">Fiji</option>
						<option value="Filipinas">Filipinas</option>
						<option value="Finlandia">Finlandia</option>
						<option value="Francia">Francia</option>
						<option value="Gabon">Gabon</option>
						<option value="Gambia">Gambia</option>
						<option value="Georgia">Georgia</option>
						<option value="Ghana">Ghana</option>
						<option value="Gibraltar">Gibraltar</option>
						<option value="Granada">Granada</option>
						<option value="Grecia">Grecia</option>
						<option value="Groenlandia">Groenlandia</option>
						<option value="Guadalupe">Guadalupe</option>
						<option value="Guam">Guam</option>
						<option value="Guatemala">Guatemala</option>
						<option value="Guayana">Guayana</option>
						<option value="Guayana Francesa">Guayana Francesa</option>
						<option value="Guinea">Guinea</option>
						<option value="Guinea Ecuatorial">Guinea Ecuatorial</option>
						<option value="Guinea-Bissau">Guinea-Bissau</option>
						<option value="Haiti">Haiti</option>
						<option value="Honduras">Honduras</option>
						<option value="Hungria">Hungria</option>
						<option value="India">India</option>
						<option value="Indonesia">Indonesia</option>
						<option value="Irak">Irak</option>
						<option value="Iran">Iran</option>
						<option value="Irlanda">Irlanda</option>
						<option value="Isla Bouvet">Isla Bouvet</option>
						<option value="Isla de Christmas">Isla de Christmas</option>
						<option value="Islandia">Islandia</option>
						<option value="Islas Caiman">Islas Caiman</option>
						<option value="Islas Cook">Islas Cook</option>
						<option value="Islas de Cocos">Islas de Cocos</option>
						<option value="Islas Faroe">Islas Faroe</option>
						<option value="Islas Heard y McDonald">Islas Heard y McDonald</option>
						<option value="Islas Malvinas">Islas Malvinas</option>
						<option value="Islas Marianas del Norte">Islas Marianas del Norte</option>
						<option value="Islas Marshall">Islas Marshall</option>
						<option value="Islas menores EU">Islas menores EU</option>
						<option value="Islas Palau">Islas Palau</option>
						<option value="Islas Salomon">Islas Salomon</option>
						<option value="Islas Svalbard y Jan Mayen">Islas Svalbard y Jan Mayen</option>
						<option value="Islas Tokelau">Islas Tokelau</option>
						<option value="Islas Turks y Caicos">Islas Turks y Caicos</option>
						<option value="Islas Virgenes (US)">Islas Virgenes (US)</option>
						<option value="Islas Virgenes (UK)">Islas Virgenes (UK)</option>
						<option value="Islas Wallis y Futuna">Islas Wallis y Futuna</option>
						<option value="Israel">Israel</option>
						<option value="Italia">Italia</option>
						<option value="Jamaica">Jamaica</option>
						<option value="Japon">Japon</option>
						<option value="Jordania">Jordania</option>
						<option value="Kazajistan">Kazajistan</option>
						<option value="Kenia">Kenia</option>
						<option value="Kirguizistan">Kirguizistan</option>
						<option value="Kiribati">Kiribati</option>
						<option value="Kuwait">Kuwait</option>
						<option value="Laos">Laos</option>
						<option value="Lesotho">Lesotho</option>
						<option value="Letonia">Letonia</option>
						<option value="Libano">Libano</option>
						<option value="Liberia">Liberia</option>
						<option value="Libia">Libia</option>
						<option value="Liechtenstein">Liechtenstein</option>
						<option value="Lituania">Lituania</option>
						<option value="Luxemburgo">Luxemburgo</option>
						<option value="Macedonia">Macedonia</option>
						<option value="Madagascar">Madagascar</option>
						<option value="Malasia">Malasia</option>
						<option value="Malawi">Malawi</option>
						<option value="Maldivas">Maldivas</option>
						<option value="Mali">Mali</option>
						<option value="Malta">Malta</option>
						<option value="Marruecos">Marruecos</option>
						<option value="Martinica">Martinica</option>
						<option value="Mauricio">Mauricio</option>
						<option value="Mauritania">Mauritania</option>
						<option value="Mayotte">Mayotte</option>
						<option value="Mexico">Mexico</option>
						<option value="Micronesia">Micronesia</option>
						<option value="Moldavia">Moldavia</option>
						<option value="Monaco">Monaco</option>
						<option value="Mongolia">Mongolia</option>
						<option value="Montserrat">Montserrat</option>
						<option value="Mozambique">Mozambique</option>
						<option value="Namibia">Namibia</option>
						<option value="Nauru">Nauru</option>
						<option value="Nepal">Nepal</option>
						<option value="Nicaragua">Nicaragua</option>
						<option value="Niger">Niger</option>
						<option value="Nigeria">Nigeria</option>
						<option value="Niue">Niue</option>
						<option value="Norfolk">Norfolk</option>
						<option value="Noruega">Noruega</option>
						<option value="Nueva Caledonia">Nueva Caledonia</option>
						<option value="Nueva Zelanda">Nueva Zelanda</option>
						<option value="Oman">Oman</option>
						<option value="Paises Bajos">Paises Bajos</option>
						<option value="Panama">Panama</option>
						<option value="Pap�a Nueva Guinea">Pap�a Nueva Guinea</option>
						<option value="Paquistan">Paquistan</option>
						<option value="Paraguay">Paraguay</option>
						<option value="Peru">Peru</option>
						<option value="Pitcairn">Pitcairn</option>
						<option value="Polinesia Francesa">Polinesia Francesa</option>
						<option value="Polonia">Polonia</option>
						<option value="Portugal">Portugal</option>
						<option value="Puerto Rico">Puerto Rico</option>
						<option value="Qatar">Qatar</option>
						<option value="Reino Unido">Reino Unido</option>
						<option value="Republica Centroafricana">Republica Centroafricana</option>
						<option value="Republica Checa">Republica Checa</option>
						<option value="Republica de Sudafrica">Republica de Sud�frica</option>
						<option value="Republica Dominicana">Republica Dominicana</option>
						<option value="Republica Eslovaca">Republica Eslovaca</option>
						<option value="Reunion">Reunion</option>
						<option value="Ruanda">Ruanda</option>
						<option value="Rumania">Rumania</option>
						<option value="Rusia">Rusia</option>
						<option value="Sahara Occidental">Sahara Occidental</option>
						<option value="Saint Kitts y Nevis">Saint Kitts y Nevis</option>
						<option value="Samoa">Samoa</option>
						<option value="Samoa Americana">Samoa Americana</option>
						<option value="San Marino">San Marino</option>
						<option value="San Vicente y Granadinas">San Vicente y Granadinas</option>
						<option value="Santa Helena">Santa Helena</option>
						<option value="Santa Lucia">Santa Lucia</option>
						<option value="Santo Tome y Principe">Santo Tome y Principe</option>
						<option value="Senegal">Senegal</option>
						<option value="Seychelles">Seychelles</option>
						<option value="Sierra Leona">Sierra Leona</option>
						<option value="Singapur">Singapur</option>
						<option value="Siria">Siria</option>
						<option value="Somalia">Somalia</option>
						<option value="Sri Lanka">Sri Lanka</option>
						<option value="St Pierre y Miquelon">St Pierre y Miquelon</option>
						<option value="Suazilandia">Suazilandia</option>
						<option value="Sudan">Sudan</option>
						<option value="Suecia">Suecia</option>
						<option value="Suiza">Suiza</option>
						<option value="Surinam">Surinam</option>
						<option value="Tailandia">Tailandia</option>
						<option value="Taiwan">Taiwan</option>
						<option value="Tanzania">Tanzania</option>
						<option value="Tayikist�n">Tayikist�n</option>
						<option value="Territorios franceses del Sur">Territorios franceses del Sur</option>
						<option value="Timor Oriental">Timor Oriental</option>
						<option value="Togo">Togo</option>
						<option value="Tonga">Tonga</option>
						<option value="Trinidad y Tobago">Trinidad y Tobago</option>
						<option value="T�nez">T�nez</option>
						<option value="Turkmenistan">Turkmenistan</option>
						<option value="Turquia">Turquia</option>
						<option value="Tuvalu">Tuvalu</option>
						<option value="Ucrania">Ucrania</option>
						<option value="Uganda">Uganda</option>
						<option value="Uruguay">Uruguay</option>
						<option value="Uzbekistan">Uzbekistan</option>
						<option value="Vanuatu">Vanuatu</option>
						<option value="Venezuela">Venezuela</option>
						<option value="Vietnam">Vietnam</option>
						<option value="Yemen">Yemen</option>
						<option value="Yugoslavia">Yugoslavia</option>
						<option value="Zambia">Zambia</option>
						<option value="Zimbabue">Zimbabue</option>
					</select>
					<span id="titulo" style="display:none"><label for="titulo">Departamento</label>
					<select name="departamento">
						<option value="Amazonas">Amazonas</option>
						<option value="Antioquia">Antioquia</option>
						<option value="Arauca">Arauca</option>
						<option value="Atlantico">Atl�ntico</option>
						<option value="Bolivar">Bol�var</option>
						<option value="Boyaca">Boyac�</option>
						<option value="Caldas">Caldas</option>
						<option value="Caquet�">Caquet�</option>
						<option value="Casanare">Casanare</option>
						<option value="Cauca">Cauca</option>
						<option value="Ces�r">Ces�r</option>
						<option value="Cordoba">C�rdoba</option>
						<option value="Cundinamarca">Cundinamarca</option>
						<option value="Choc�">Choc�</option>
						<option value="Guain�a">Guain�a</option>
						<option value="Guajira">Guajira</option>
						<option value="Guaviare">Guaviare</option>
						<option value="Huila">Huila</option>
						<option value="Magdalena">Magdalena</option>
						<option value="Meta">Meta</option>
						<option value="Nari�o">Nari�o</option>
						<option value="N.Santander">N.Santander</option>
						<option value="Putumayo">Putumayo</option>
						<option value="Quind�o">Quind�o</option>
						<option value="Risaralda">Risaralda</option>
						<option value="San Andres">San Andres</option>
						<option value="Santander">Santander</option>
						<option value="Sucre">Sucre</option>
						<option value="Tolima">Tolima</option>
						<option value="Valle">Valle Del Cauca</option>
						<option value="Vaup�s">Vaup�s</option>
						<option value="Vichada">Vichada</option>
					</select>
					</span>
					</td>
					
					<td>Ciudad / City <font color="red">*</font></td>
					<td colspan="2"> <input type="text"  size=300 style="width:300px" name="ciudad" value="<?php echo $_POST['ciudad']; ?>"></td>
				</tr>
				<tr>
					<td>Tel&eacute;fono / Phone <font color="red">*</font></td>
					<td> <input type="text"  size=300 style="width:300px" name="telefono" value="<?php echo $_POST['telefono']; ?>"></td>					
					<td>Correo electr&oacute;nico / e-mail <font color="red">*</font></td>
					<td colspan="2"> <input type="text"  size=300 style="width:300px" name="mail" value="<?php echo $_POST['mail']; ?>"></td>

				</tr>
				
			</table>
			<table>	
				<tr>
					<td align="center" colspan='2'>
					<b>2. OBJETO DE LA SOLICITUD / SUBJECT <font color="red">*</font></b>
					</td>
				
				</tr>
				<tr>
					<td> <input type="radio" name="compra" value="compra" <?php if ($_POST['compra']=='compra'){echo 'checked';} else {} ?> />
					Compra / Final</td>
					
					<td> <input type="radio" name="compra" value="presupuesto" <?php if ($_POST['compra']=='presupuesto'){echo 'checked';} else {} ?> />
					Presupuesto / Budgetary</td>
					
				</tr>	
			</table>

			<table>	
				<tr>
				<td  align="center" colspan='7'><b>3. SOLICITUD / REQUIREMENT</b></td>
				</tr>
				<tr>
				<td align="center" colspan='7'>3.1 REQUERIMIENTOS T&Eacute;CNICOS / TECHNICAL REQUIREMENTS</td>
				</tr>

			</table>	
			<table border= '3'>
				<tr>
					<td> Cantidad / Quantity <font color="red">*</font></td>
					<td> Potencia(kVA) / Power(kVA) <font color="red">*</font></td>
					<td> Fases / Phases <font color="red">*</font></td>
					<td> Voltaje primario(V) / High voltage(V) <font color="red">*</font></td>
					<td> Voltaje secundario(V) / Low voltage(V) <font color="red">*</font></td>
					<td> Tipo / Type � <font color="red">*</font></td>
					<td> Norma y/o especificaci&oacute;n / Standard/Specification <font color="red">*</font></td>
				</tr>
				<?php
				for ($i=1;$i<=7;$i++)
				{
				?>
				<tr>
				<td> <input type="text" size="10" maxlength="10" name=<?php echo "cantidad$i"; ?> value=<?php echo $_POST["cantidad$i"];?> > </td>
				<td> 
						<select name= <?php echo "potencia$i"; ?>> 
						<option></option>
						<option>	3	</option>
						<option>	5	</option>
						<option>	7,5	</option>
						<option>	10	</option>
						<option>	15	</option>
						<option>	16	</option>
						<option>	20	</option>
						<option>	25	</option>
						<option>	30	</option>
						<option>	37,5</option>
						<option>	40	</option>
						<option>	45	</option>
						<option>	50	</option>
						<option>	60	</option>
						<option>	63	</option>
						<option>	75	</option>
						<option>	80	</option>
						<option>	100	</option>
						<option>	112,5	</option>
						<option>	125	</option>
						<option>	150	</option>
						<option>	160	</option>
						<option>	167	</option>
						<option>	167,5	</option>
						<option>	175	</option>
						<option>	200	</option>
						<option>	225	</option>
						<option>	250	</option>
						<option>	260	</option>
						<option>	300	</option>
						<option>	315	</option>
						<option>	330	</option>
						<option>	333	</option>
						<option>	350	</option>
						<option>	400	</option>
						<option>	500	</option>
						<option>	520	</option>
						<option>	600	</option>
						<option>	630	</option>
						<option>	667	</option>
						<option>	750	</option>
						<option>	800	</option>
						<option>	833	</option>
						<option>	850	</option>
						<option>	1000	</option>
						<option>	1200	</option>
						<option>	1250	</option>
						<option>	1500	</option>
						<option>	1600	</option>
						<option>	1800	</option>
						<option>	2000	</option>
						<option>	2500	</option>
						<option>	3000	</option>
						<option>	3500	</option>
						<option>	3750	</option>
						<option>	4000	</option>
						<option>	5000	</option>
						<option>	6000	</option>
						<option>	7500	</option>
						<option>	8000	</option>
						<option>	10000	</option>
						</select>
				</td>
				<td> 
						<select name= <?php echo "fases$i"; ?>> 
						<option></option>
						<option>1</option>
						<option>3</option>
						</select>
				<td> 
						<select name= <?php echo "vp$i"; ?>> 
						<option></option>
						<option>	208	</option>
						<option>	220	</option>
						<option>	226	</option>
						<option>	240	</option>
						<option>	380	</option>
						<option>	408	</option>
						<option>	440	</option>
						<option>	460	</option>
						<option>	480	</option>
						<option>	500	</option>
						<option>	550	</option>
						<option>	1000</option>
						<option>	2400	</option>
						<option>	3300	</option>
						<option>	4160	</option>
						<option>	4800	</option>
						<option>	6000	</option>
						<option>	6300	</option>
						<option>	6600	</option>
						<option>	6900	</option>
						<option>	7200	</option>
						<option>	7620	</option>
						<option>	10000	</option>
						<option>	11400	</option>
						<option>	11500	</option>
						<option>	11800	</option>
						<option>	12000	</option>
						<option>	12470	</option>
						<option>	12700	</option>
						<option>	13200	</option>
						<option>	13550	</option>
						<option>	13800	</option>
						<option>	14400	</option>
						<option>	14800	</option>
						<option>	20000	</option>
						<option>	22000	</option>
						<option>	22860	</option>
						<option>	22900	</option>
						<option>	23000	</option>
						<option>	27300	</option>
						<option>	33000	</option>
						<option>	34500	</option>
						<option>	12470GrdY/7200	</option>
						<option>	12470Y/7200	</option>
						<option>	13200/22860Y	</option>
						<option>	13200-11400	</option>
						<option>	13200-11400	</option>
						<option>	13200GrdY/7620	</option>
						<option>	13800/23900Y	</option>
						<option>	13800GrdY/7967	</option>
						<option>	14400/24940Y	</option>
						<option>	19052/33000Y	</option>
						<option>	19920/34500Y	</option>
						<option>	220/127	</option>
						<option>	22000 GrdY/12700	</option>
						<option>	22860GrdY/13200	</option>
						<option>	23900Grdy/13800	</option>
						<option>	2400/4160Grdy  X  7200/12470Grdy	</option>
						<option>	2400/4160Y	</option>
						<option>	24940GrdY/14400	</option>
						<option>	24940Y/14400	</option>
						<option>	33000GrdY/19052	</option>
						<option>	34500GrdY/19920	</option>
						<option>	4160/7200Y	</option>
						<option>	4160GrdY/2400	</option>
						<option>	440Y/254	</option>
						<option>	480/277	</option>
						<option>	4800/8320Y	</option>
						<option>	4800D - 12470 Y / 7200	</option>
						<option>	7200/12470Y	</option>
						<option>	7620/13200Y	</option>
						<option>	7970/13800Y	</option>
						<option>	8314/14400Y 	</option>

						</select>
				</td>
				<td> 
						<select name= <?php echo "vs$i"; ?>> 
						<option></option>
						<option>	208	</option>
						<option>	220	</option>
						<option>	230	</option>
						<option>	240	</option>
						<option>	277	</option>
						<option>	420	</option>
						<option>	440	</option>
						<option>	480	</option>
						<option>	4160	</option>
						<option>	6000	</option>
						<option>	6300	</option>
						<option>	7200	</option>
						<option>	7620	</option>
						<option>	7967	</option>
						<option>	10000	</option>
						<option>	11400	</option>
						<option>	12000	</option>
						<option>	12470	</option>
						<option>	12700	</option>
						<option>	13200	</option>
						<option>	13800	</option>
						<option>	22000	</option>
						<option>	22860	</option>
						<option>	22900	</option>
						<option>	34500	</option>
						<option>	1000/577	</option>
						<option>	1100 to 3800	</option>
						<option>	1100 to 4100	</option>
						<option>	11400Y/6582	</option>
						<option>	120-240	</option>
						<option>	13200/7620	</option>
						<option>	13800/7967	</option>
						<option>	15000-4000	</option>
						<option>	1600 to 5400	</option>
						<option>	208/120	</option>
						<option>	210/121	</option>
						<option>	214/123	</option>
						<option>	215/124	</option>
						<option>	217/125	</option>
						<option>	220/127	</option>
						<option>	220-110	</option>
						<option>	220Y/127x440Y/254	</option>
						<option>	225/130	</option>
						<option>	228/132	</option>
						<option>	230/132	</option>
						<option>	230-460	</option>
						<option>	2400/1386	</option>
						<option>	240-120	</option>
						<option>	240-480	</option>
						<option>	246-123	</option>
						<option>	250/800/2500 	</option>
						<option>	30000/17320	</option>
						<option>	380/219	</option>
						<option>	400/230	</option>
						<option>	4160/2400	</option>
						<option>	4160/2400 � 440/254	</option>
						<option>	433/250	</option>
						<option>	440/254	</option>
						<option>	445/257	</option>
						<option>	446/257	</option>
						<option>	460/265	</option>
						<option>	464/268	</option>
						<option>	470/271	</option>
						<option>	480/277	</option>
						<option>	480/480	</option>
						<option>	480-240	</option>
						<option>	6600/3810	</option>
						<option>	6900/3984	</option>
						</select>
				</td>
				<td>	
						<select name= <?php echo "tipo$i"; ?>> 
							<option></option>
							<option>	AUTOPROTEGIDO CSP	</option>
							<option>	AUTOTRANSFORMADOR DESFASADOR 	</option>
							<option>	CAJA DE CONEXIONES	</option>
							<option>	CAJA DE MANIOBRA	</option>
							<option>	CONVENCIONAL	</option>
							<option>	FRECUENCIA VARIABLE	</option>
							<option>	OCASIONALMENTE SUMERGIBLE RADIAL	</option>
							<option>	OCASIONALMENTE SUMERGIBLE MALLA	</option>
							<option>	PEDESTAL MALLA	</option>
							<option>	PEDESTAL MALLA AUTOPROTEGIDA	</option>
							<option>	PEDESTAL RADIAL	</option>
							<option>	PEDESTAL RADIAL AUTOPROTEGIDA	</option>
							<option>	PHASE SHIFT TRANSFORMER PST	</option>
							<option>	SECO CLASE F (RESINA EP�XICA)	</option>
							<option>	SECO CLASE H (BARNIZ)	</option>
							<option>	STEP DOWN TRANSFORMER SDT	</option>
							<option>	STEP UP TRANSFORMER SUT	</option>
							<option>	TIPO K (FACTOR DE ARM�NICOS #1)	</option>
						</select>
				</td>
				<td> <input type="text" maxlength="100" name=<?php echo "norma$i"; ?>> </td>
				</tr>
				<?php	
				}
				
				?>
				
				<tr>
					<td align="center" colspan='7'>
					<i>� Tipo: transformador convencional sumergido en aceite, seco, ocasionalmente sumergible radial, ocasionalmente sumergible malla, pedestal anillo, pedestal radial, etc </i>
					</td>
				</tr>	
				<tr>
					<td align="center" colspan='7'>
					<i>� Type: Liquid-Immersed Distribution, dry , Loop feed, pad mounted, radial feed pad mounted, Subway type Networks, Vault Type Networks</i>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td>3.11 OTROS REQUERIMIENTOS T�CNICOS  / OTHER </td>
				</tr>	
				<tr>
					<td><TEXTAREA name="otros", ROWS=4, COLS=150><?php echo $_POST['otros'];?></TEXTAREA></td>
				</tr>
			</table>
			<table border= '1'>
				<tr>
					<td align="center" colspan='4'>
					3.2 REQUERIMIENTOS COMERCIALES / COMMERCIAL REQUIREMENTS
					</td>
				
				</tr>
				<tr>
					<td align="center" colspan='1'>Forma de pago / Payment terms </td>
					<td align="center" colspan='3'><input type="text"  size=100 style="width:930px" name="formapago" value="<?php echo $_POST['formapago']; ?>"></td>
					
				
				</tr>
				<tr>
					<td>Plazo de entrega / Delivery term </td>
					<td> <input type="text"  size=300 style="width:300px" name="plazo" value="<?php echo $_POST['plazo']; ?>"></td>
					
					<td>Sitio de entrega / Site of delivery </td>
					<td> <input type="text"  size=300 style="width:300px" name="sitio" value="<?php echo $_POST['sitio']; ?>"></td>
				</tr>
				<tr>
					<td>Garantia / Warranty </td>
					<td> <input type="text"  size=300 style="width:300px" name="garantia" value="<?php echo $_POST['garantia']; ?>"></td>
					
					<td>Validez / Validity </td>
					<td> <input type="text"  size=300 style="width:300px" name="validez" value="<?php echo $_POST['validez']; ?>"></td>
				</tr>
			</table>
			</td>
	</tr>
	<tr>
	<td><input type="submit" align="center" colspan="4" name="ENVIAR" VALUE="ENVIAR"></td>
	</tr>
</table>
</FORM>




</body>


<?php
if (isset($_POST['ENVIAR']))
	{
	//Se toma la hora y la fecha para registrar en la solicitud
	date_default_timezone_set('America/Bogota');
	$fecha= date("Y-m-d H:i:s");
	// Se hacen las validaciones del formulario
	//************************************************
		if ($_POST['empresa']=='' or $_POST['contacto']=='' or $_POST['ciudad']=='' or $_POST['pais']=='' or $_POST['telefono']=='' or $_POST['mail']=='' or $_POST['compra']==''
			or $_POST["cantidad1"] == '' or $_POST["potencia1"] == '' or $_POST["fases1"] == '' or $_POST["vp1"] == '' or $_POST["vs1"] == ''
			or $_POST["tipo1"] == '' or $_POST["norma1"] == '')
				{
				?>
		
					<script language='javascript'>
					alert('los campos con (*) son obligatorios / Fields with (*) are mandatory');
					</script>
		
				<?php
		
				}		
		else	
				{
					if ($_POST['pais']!='Colombia')
					 {
					  $_POST['departamento']="No aplica";
					 }
				
				// Se insertan los datos del formulario en la BD
				//************************************************						
				include_once('conexion.php');
				$consulta = "INSERT INTO solcotizaciones (cia,contacto,pais,depto,ciudad,tel,mail,objeto,otros,formapago,plazo,garantia,sitioentrega,validez,fecha) VALUES('".$_POST['empresa']."','";
				$consulta.=  $_POST['contacto']."','".$_POST['pais']."','".$_POST['departamento']."','".$_POST['ciudad']."','".$_POST['telefono']."','".$_POST['mail']."','".$_POST['compra']."','";
				$consulta.=	 $_POST['otros']."','".$_POST['formapago']."','".$_POST['plazo']."','".$_POST['garantia']."','".$_POST['sitio']."','".$_POST['validez']."','".$fecha."')";
				
				//valido que los items solicitados existan y los meto en la BD
				if(mysql_query($consulta,$conexion))
					{
						$idsolicitud = mysql_insert_id();
						$contador=0;
						for($i=1;$i<=7;$i++)
						{
						    if ($_POST["cantidad$i"] == '')
							{
							$contador++;
							}
							else
							{
								$consulta2  = "INSERT INTO itemscotizacion (cantidad,potencia,fases,vp,vs,tipo,norma,idcotizacion)";
								$consulta2.= " VALUES('".$_POST["cantidad$i"]."','".$_POST["potencia$i"]."','".$_POST["fases$i"]."','";
								$consulta2 .= $_POST["vp$i"]."','".$_POST["vs$i"]."','".$_POST["tipo$i"]."','".$_POST["norma$i"]."','".$idsolicitud."')" ;
								mysql_query($consulta2,$conexion);
									
							}
								
						}
						if ($contador == 7)	
						{
							?>
							<script language='javascript'>
							alert('Debe digital al menos un item a cotizar');
							</script>
							<?php
							$consulta3 = "DELETE FROM solcotizaciones WHERE numsolicitud = '$idsolicitud'";
							mysql_query($consulta3,$conexion);
							/*$autoincremento = 20;
							echo $autoincremento;
							$consulta4 = "ALTER TABLE solcotizaciones AUTO_INCREMENT=$autoincremento";
							mysql_query($consulta4,$conexion);*/
						}
						else if($contador < 7)
						{
							
							?>
							<script language='javascript'>
							alert('Su solicitud se ha enviado');
							</script>
							<?php
							//header("http://www.magnetron.com.co/magnetron/F-PV2-13/formulario_prueba.php");
							#----------------------------------------------------------#
							#Aqu� se define aquien va dirigido el correo
							#----------------------------------------------------------#
							if($_POST['pais']=="Panama")
							  {
							 #mandar a roberto Arosemena
							 $destinatario="cotizacionespanama@magnetron.com.co";
							  }
							else if($_POST['pais']=="Colombia")
							{
							 if($_POST['departamento']=="Valle" or $_POST['departamento']=="Cauca")
							  {
								#mandar a oficina Cali
 							    $destinatario="cotizacionescali@magnetron.com.co";
							  }
							  else if ($_POST['departamento']=="Boyaca" or $_POST['departamento']=="Arauca" 
									  or $_POST['departamento']=="Cundinamarca" or $_POST['departamento']=="Casanare")
							  {
								#mnadar a oficina Bogota
								$destinatario="cotizacionesbogota@magnetron.com.co";
							  }
							  else if ($_POST['departamento']=="Guajira" or $_POST['departamento']=="Magdalena" 
									  or $_POST['departamento']=="Atlantico" or $_POST['departamento']=="Cordoba"
									  or $_POST['departamento']=="Bolivar" or $_POST['departamento']=="Sucre")
							  {
								#mnadar a oficina Barranquilla
								$destinatario="cotizacionesabq@magnetron.com.co";
							  }
							  	else
								 {
								  #Mandar a cotizaciones";
								  $destinatario="avalencia@magnetron.com.co";
								
								 }
							}
							else
							{
							 #Mandar a cotizaciones";
							 $destinatario="avalencia@magnetron.com.co";
							}
							
							#----------------------------------------------------------#
							#--------------------ENVIA EL CORREO-----------------------#
							/*
							ini_set("SMTP","aspmx.l.google.com");
							ini_set('smtp_port',25);
							ini_set('sendmail_from', 'tics@magnetron.com.co');
							$asunto = "Solicitud de cotizacion Web No.".$idsolicitud;
							$mensaje = "El Se�or(a) ".$_POST['contacto']." de la empresa: ".$_POST['empresa'];
							$mensaje .= " de la ciudad de ".$_POST['ciudad']." ha solicitado una cotizacion. Para ver los ";
							$mensaje .= "detalles haga clic en el siguiente enlace: ";
							$mensaje .= "http://magnetron.com.co/magnetron/F-PV2-13/detalle.php?id=".$idsolicitud;
							//mail("cvillada@magnetron.com.co",$asunto,$mensaje); 
							mail($destinatario,$asunto,$mensaje,"From: SolicitudesWeb<tics@magnetron.com.co>"."\n\r"."Reply-To: ".$_POST['mail']); 
							*/
							#-----------------------------------------------------------#
							




							include('PHPMailer/class.phpmailer.php');
							include('PHPMailer/class.pop3.php');
							include('PHPMailer/class.smtp.php');
							$email = "avalencia@magnetron.com.co";
							$clave = "Ajvalencia_86";
							$user = "Adriana";
							$correo = "avalencia@magnetron.com.co";		
						
							/*$clave = $reg[4];
							$user = $reg[3];
							$correo = $reg[5];
							*/
							$mail = new PHPMailer();
							$mail->IsSMTP();
							$mail->SMTPAuth = true;
							$mail->SMTPSecure = "ssl";
							$mail->Host = "aspmx.l.google.com"; // SMTP a utilizar. Por ej. smtp.elserver.com
							$mail->Username = "avalencia@magnetron.com.co"; // Correo completo a utilizar
							$mail->Password = "Ajvalencia_86"; // Contrase�a
							$mail->Port = 25; // Puerto a utilizar

							//Con estas pocas l�neas iniciamos una conexi�n con el SMTP. Lo que ahora deber�amos hacer, es configurar el mensaje a enviar, el //From, etc.
							$mail->From = "tics@magnetron.com.co"; // Desde donde enviamos (Para mostrar)
							$mail->FromName = "Servidor de Correo";

							//Estas dos l�neas, cumplir�an la funci�n de encabezado (En mail() usado de esta forma: �From: Nombre <correo@dominio.com>�) de //correo.
							$mail->AddAddress($correo); // Esta es la direcci�n a donde enviamos
							$mail->IsHTML(true); // El correo se env�a como HTML
							$mail->Subject = $asunto; // Este es el titulo del email.
							$body = $mensaje;
							$mail->Body = $body; 
							if (!$mail->Send())
							{
								//echo "mira a ver";//
								echo $mail->ErrorInfo;
								echo "<script>";
								echo "alert('��� Error de envio !!!')"; //muestra mensaje de error
								echo "</script>";	
								//echo "<meta http-equiv='Refresh' content='1; url=html5.html'>"; //redireccionamos a la p�gina
							}
							else
							{
								echo "<script>";
								echo "alert('��� Su clave fue enviado al correo, revisar por favor !!!')"; //muestra mensaje de error
								echo "</script>";	
								
							}

						}
					}

				else
					{
						?>
							<script language='javascript'>
							alert('No se pudo enviar la informaci�n');
							</script>
						<?php
					}
				
				}
	

	}
?>
</html> 