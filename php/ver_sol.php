<?php
  include "conn.php";

  $sol=$_REQUEST['sol'];

  $sql=mysqli_query($conn,"SELECT * FROM v_sol_fun_est WHERE id_solicitud=$sol");
  $row_sql1=mysqli_fetch_array($sql);
  $estado=$row_sql1[10];
?>
<br>
<div class="col-md-12 col-md-offset-0 "><!--vcenter-->
  <h1 class="text-center">Solicitud N° <?php echo $sol; ?></h1><br><br>

  <section class="container row center-block">
    
      <div class="col-xs-12 col-sm-12 col-md-10"><!-- LISTA DE SOLICITUDES -->
        <div class="col-md-12">
          <div class="form-group">
            <label for="objeto" class="control-label col-md-4">Usuario: </label>
            <label for="objeto" class="control-label col-md-8"><?php echo $row_sql1[6]; ?></label>
          </div><br>
          <div class="form-group">
            <label for="objeto" class="control-label col-md-4">Hora:Fecha Solicitud: </label>
            <label for="objeto" class="control-label col-md-8"><?php echo $row_sql1[4]; ?></label>
          </div><br>
          <div class="form-group">
            <label for="objeto" class="control-label col-md-4">Estado: </label>
            <?php
              if ($estado!="Ejecutado") {
                echo "<label for=\"objeto\" class=\"control-label col-md-8 text-success\">".$row_sql1[10]."</label>";
              }else{
                echo "<label for=\"objeto\" class=\"control-label col-md-8 text-danger\">".$row_sql1[10]."</label>";
              }
            ?>
          </div><br>

          <div class="form-group">
            <label for="objeto" class="control-label col-md-4">Observación de Solicitud: </label>
            <label for="objeto" class="control-label col-md-8"><?php echo $row_sql1[3]; ?></label>
          </div>

          <div class="form-group">
            <label for="objeto" class="control-label col-md-4">Documentos: </label>
          <?php
            //mysqli_free_result($sql);

            $sql2=mysqli_query($conn,"SELECT * FROM t_docs_solicitud WHERE t_solicitud_id_solicitud=$sol");
          ?>

          <table class="">            
            <?php
            while ($row_sql2=mysqli_fetch_array($sql2)) {
              echo "<tr>
                      <td><label for=\"objeto\" class=\"control-label col-md-12 text-success\"> - ".$row_sql2[2]."</td>
                    </tr>";
            }
            ?>
            
          </table>
          </div>

          <?php
          if ($estado!="Ejecutado") {
            ?>
          <form action="management.php?pag=funct" method="POST">
            <div class="form-group">
              <label for="objeto" class="control-label col-sm-12 col-md-4">Observación de Entrega</label>
              <div class="col-sm-12 col-md-8">
                <textarea class="form-control" name="observacion_entrega" value="">
                </textarea>
              </div>
            </div>
            <br><br><br>

            <div class="form-group">
            <label for="objeto" class="control-label col-md-4">Estado de Entrega <b>*</b></label>
            <div class="col-md-5">
              <select class="form-control" name="sol_estado" required>
                <option value="">Seleccione...</option>
                <option value="2">Ejecutado</option>
                <option value="3">No Disponible</option>
                <option value="4">No Permitido</option>
              </select>
            </div>
          </div>
          <br><br>
            <?php
          }else{
            ?>
            <div class="form-group">
              <label for="objeto" class="control-label col-md-4">Observación de Entrega</label>
              <label for="objeto" class="control-label col-md-8"><?php echo $row_sql1[11]; ?></label>
            </div>

             <div class="form-group">
              <label for="objeto" class="control-label col-md-4">Hora:Fecha Ejecución</label>
              <label for="objeto" class="control-label col-md-8"><?php echo $row_sql1[5]; ?></label>
            </div><br>
            <br><br>
            <?php
          }
          ?>

          <div class="col-md-12 text-right">
            <?php 
            if ($estado!="Ejecutado") {
              ?>
              <input type="hidden" value="ent_sol" name="opcion">
              <?php echo "<input type=\"hidden\" value=\"".$sol."\" name=\"sol\">"; ?>

              <button type="submit" class="btn btn-success btn-sm" >Entregar</button>
              <?php
              //echo "<a href=\"management.php\" class=\"btn btn-sm btn-success\"> Entregar</a>";
            }
             ?>
            <a href="management.php" class="btn btn-sm btn-success"> Volver</a>
          </div>

        </form>
        </div>
      </div>
  </section>
</div>
<br><br><br><br><br>