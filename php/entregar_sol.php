<?php
  include "conn.php";
  
  if (isset($_REQUEST['sol'])) {
    $sol=$_REQUEST['sol'];
  }else{
    $sol="";
  }

  if (isset($_REQUEST['busca'])) {
    $busca=$_REQUEST['busca'];
    if ($busca=="") {
      $buscar="AND 1";
    }else{
      $buscar="AND id_solicitud=$busca";
    }
  }else{
    $buscar="AND 1";
  }

  $sql=mysqli_query($conn,"SELECT * FROM v_sol_fun_est vf JOIN t_usuario tu ON(vf.t_usuario_idusuario=tu.idusuario) WHERE idestado_solicitud<>1 $buscar");
  $num=mysqli_num_rows($sql);
?>
<div class="col-sm-12 col-md-11 col-md-offset-1">
  <h1 class="text-center">Entregas<br><small><?php echo $num; ?> Resultados</small></h1><br>
</div>
  <section class="container-fluid row ">
    <form action="management.php?pag=entregar_sol" method="POST" class="form-group">
      <div class="form-group"  class="col-md-12 text-left">
        <label for="nom_solicitante" class="control-label col-sm-2 col-md-1 text-right">Buscar</label><!-- funcionarios de Ingenieria -->

        <div class="col-xs-11 col-sm-5 col-md-3">
          <input type="text" id="busca" name="busca" class="form-control" placeholder="Buscar Solicitud N°...">
        </div>

        <div class="col-xs-1 col-sm-2 col-md-1 text-left">
          <button type="submit" class="btn btn-success btn-sm">ir..</button>
        </div>
      </div>
    </form>
      <br><br>

      <div class="col-xs-12 col-sm-12 col-sm-offset-1 col-md-12 col-md-offset-1 col-lg-10"><!-- LISTA DE SOLICITUDES -->
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-12">
          <table class="table">
            <th>Solicitud Nro.</th><th>Funcionario</th><th>Documento(s)</th><th>Tipo Archivo</th><th>Hora:Fecha Solicitud</th><th>Estado</th><th>Hora:Fecha Cierre</th><th>Quien Entregó</th><th>Detalles</th>
            <?php
            while ($row_sql=mysqli_fetch_array($sql)) {
              echo "<tr>
                      <td>".$row_sql[0]."</td><td>".$row_sql[6]."</td><td>".$row_sql[1]."</td><td>".$row_sql[2]."</td><td>".$row_sql[4]."</td><td>".$row_sql[10]."</td><td>".$row_sql[5]."</td><td>".$row_sql[13]."</td>
                      <td><a href=\"management.php?pag=ver_sol&sol=".$row_sql[0]."\" class=\"btn btn-xs btn-danger\">Ver</a></td>
                    </tr>";
            }
            ?>
          </table>
          <br>
          <a href="management.php" class="btn btn-sm btn-success"> Volver</a>
        </div>
      </div>
  </section>
<br><br><br><br><br>