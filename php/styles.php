
<!-- ESTILOS Y FUENTES Y SCRIPTS COMUNES PARA EL APLICATIVO-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Cubicador de Mercancia">
    <meta name="author" content="Jeison Stevens Cordoba">
    
    <link rel="icon" href="img/favicon.ico">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">

    <script type="text/javascript" src="bootstrap/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/styles.js"></script>

    <title>Solar - Magnetron</title>

<?php
//    require "adm_pag.php";//llama al administrador de paginas
?>
