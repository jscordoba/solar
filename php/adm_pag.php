<?php
//--------------------- FUNCIONES PARA LLAMAR LOS COMPONENTES DEL SITIO------------------------
    function pag($pag)
    {
        switch ($pag) {
            case 'nav-bar':
                require "php/nav-bar.php";
                break;
            case 'footer':
                require "php/footer.php";
                break;
            case 'inicio':
                require "php/inicio.php";
                break;
            case 'funct':
                require "php/sql.php";
                break;
            case 'solicitar_doc':
                require "php/solicitar_doc.php";
                break;
            case 'ver_sol':
                require "php/ver_sol.php";
                break;
            case 'entregar_sol':
                require "php/entregar_sol.php";
                break;
            case 'entrega_sol':
                require "php/entrega_sol.php";
                break;
            case 'logout':
                require "php/logout.php";
                break;
            default:
                # code... 
                # echo "<a href=\"management.php?pag=funct&sol=$sol&opcion=ent_sol\" class=\"btn btn-xs btn-success\"> Entregar</a>";
                break;
        }
    }
    
?>
<!--#############################--><!--#############################-->