<?php
  include "conn.php";

  $sol=$_REQUEST['sol'];

  $sql=mysqli_query($conn,"SELECT * FROM v_sol_fun_est WHERE id_solicitud=$sol");
  $row_sql1=mysqli_fetch_array($sql);
  $estado=$row_sql1[10];
?>
<div class="col-md-12 col-sm-12 col-md-offset-0 vcenter">
  <h1 class="text-center">Solicitud N° <?php echo $sol; ?></h1><br><br>

  <section class="container row center-block">
    
      <div class="col-sm-12 col-md-12"><!-- LISTA DE SOLICITUDES -->
        <div class="col-md-12">
          <div class="form-group">
            <label for="objeto" class="control-label col-md-2">Usuario</label>
            <label for="objeto" class="control-label col-md-3"><?php echo $row_sql1[6]; ?></label>
          </div><br>
          <div class="form-group">
            <label for="objeto" class="control-label col-md-2">Hora:Fecha Solicitud</label>
            <label for="objeto" class="control-label col-md-2"><?php echo $row_sql1[4]; ?></label>
          </div><br>
          <div class="form-group">
            <label for="objeto" class="control-label col-md-2">Estado</label>
            <?php
              if ($estado!="Ejecutado") {
                echo "<label for=\"objeto\" class=\"control-label col-md-2 text-success\">".$row_sql1[10]."</label>";
              }else{
                echo "<label for=\"objeto\" class=\"control-label col-md-2 text-danger\">".$row_sql1[10]."</label>";
              }
            ?>
          </div><br>

          <div class="form-group">
            <label for="objeto" class="control-label col-md-2">Observaciones</label>
            <label for="objeto" class="control-label col-md-4"><?php echo $row_sql1[3]; ?></label>
          </div><br>

          <div class="form-group">
            <label for="objeto" class="control-label col-md-2">Documentos</label>
          

          <?php
            //mysqli_free_result($sql);

            $sql2=mysqli_query($conn,"SELECT * FROM t_docs_solicitud WHERE t_solicitud_id_solicitud=$sol");
          ?>

          <table class="">            
            <?php
            while ($row_sql2=mysqli_fetch_array($sql2)) {
              echo "<tr>
                      <td><label for=\"objeto\" class=\"control-label col-md-12 text-success\"> - ".$row_sql2[2]."</td>
                    </tr>";
            }
            ?>
            
          </table>
          </div><br>

          <div class="form-group">
            <label for="objeto" class="control-label col-sm-12 col-md-2">Observación de Entrega</label>
            <div class="col-sm-12 col-md-4">
              <textarea class="form-control" name="observacion_entrega" value="">
              </textarea>
            </div>
          </div>
          

          <div class="form-group">
            <label for="objeto" class="control-label col-sm-12 col-md-4">Tipo de Archivo *</label>
            <div class="col-sm-12 col-md-8">
              <select class="form-control" name="tipo_archivo" required>
                <option default>Seleccione</option>
                <option value="Tecnico">Técnico</option>
                <option value="Comercial">Comercial</option>
              </select>
            </div>
          </div>
          <br>

          <div class="col-md-12">
            <?php 
            if ($estado!="Ejecutado") {
              echo "<a href=\"management.php?pag=funct&sol=$sol&opcion=ent_sol\" class=\"btn btn-xs btn-success\"> Entregar</a>";
            }
             ?>
            <a href="management.php" class="btn btn-xs btn-success"> Volver</a>
          </div>
        </div>
      </div>
  </section>
</div>
<br><br><br><br><br>