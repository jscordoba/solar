<?php
  include "conn.php";
  $sql1=mysqli_query($conn,"SELECT * FROM v_sol_fun_est WHERE idestado_solicitud=1 ORDER BY fecha_solicitud");
  $num_total=mysqli_num_rows($sql1);

  $sql=mysqli_query($conn,"SELECT * FROM v_sol_fun_est WHERE idestado_solicitud=1 ORDER BY fecha_solicitud ASC LIMIT 8");
?>
<div class="col-md-12 col-md-offset-0 text-center vcenter">
 <h1 class="">Solicitud Archivo<br><small><?php echo $num_total; ?> Resultados</small></h1><br>

  <section class="container-fluid row center-block">
    
      <div class="col-sm-9 col-md-9"><!-- LISTA DE SOLICITUDES -->
        <div class="col-md-12">
          <table class="table">
            <th>Solicitud Nro.</th><th>Funcionario</th><th>Documento(s)</th><th>Tipo Archivo</th><th>Hora:Fecha Solicitud</th><th>Estado</th><th>Detalles</th>
            <?php
            while ($row_sql=mysqli_fetch_array($sql)) {
              echo "<tr>
                      <td>".$row_sql[0]."</td><td>".$row_sql[6]."</td><td>".$row_sql[1]."</td><td>".$row_sql[2]."</td><td>".$row_sql[4]."</td><td>".$row_sql[10]."</td>
                      <td><a href=\"management.php?pag=ver_sol&sol=".$row_sql[0]."\"><button class=\"btn btn-success btn-xs\">Ver</button></a>
                    </tr>";
                    //<a href=\"management.php?pag=funct&sol=".$row_sql[0]."&opcion=ent_sol\" class=\"btn btn-xs btn-success\"> Entregar</a></td>
                    //<a href=\"management.php?pag=entrega_sol&sol=".$row_sql[0]."\" class=\"btn btn-xs btn-success\"> Entregar</a></td>
            }
            ?>
          </table>

            <!--
            <nav class="text-center">
              <ul class="pagination pagination-md">
                <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                <li class="active"><a href="">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="">4</a></li>
                <li><a href="">5</a></li>
                <li><a href=""><span aria-hidden="true">&raquo;</span></a></li>
              </ul>
            </nav>
          -->
        </div>
      </div>

      <div class="col-sm-12 col-md-3 "><!-- MENU DE OPCIONES -->
        <div class="col-sm-4 col-md-12 text-right form-group">
          <a href="management.php?pag=solicitar_doc" class="btn btn-lg btn-success text-block"><span class="glyphicon glyphicon-pushpin"></span> Solicitar Documentos</a><!-- ir a pagina de cruce de ambas BD (Mostrar BD Temp) -->
        </div>
        <div class="col-sm-4 col-md-12 text-right form-group">
          <a href="management.php?pag=entregar_sol" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-eye-open"></span> Entregas</a><!-- ir a pagina de seguimiento (Mostrar BD seguimiento) -->
        </div>
        <div class="col-sm-4 col-md-12 text-right form-group">
          <a href="management.php?pag=logout" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-remove"></span> Salir</a><!-- Logout -->
        </div>
      </div>

  </section>
</div>
