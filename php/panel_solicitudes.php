<?php
  include "conn.php";
  $sql=mysqli_query($conn,"SELECT * FROM v_sol_fun_est WHERE idestado_solicitud<>2");

?>
<div class="col-md-12 col-md-offset-0 text-center vcenter">
  <h1 class="">Solicitud Archivo</h1><br><br>

  <section class="container row center-block">
    
      <div class="col-sm-12 col-md-12"><!-- LISTA DE SOLICITUDES -->
        <div class="col-md-12">
          <table class="table">
            <th>Solicitud Nro.</th><th>Usuario</th><th>Documento(s)</th><th>Hora:Fecha Solicitud</th><th>Estado</th><th>Hora:Fecha Cierre</th><th>Detalles</th>
            <?php
            while ($row_sql=mysqli_fetch_array($sql)) {
              echo "<tr>
                      <td>".$row_sql[0]."</td><td>".$row_sql[10]." ".$row_sql[11]."</td><td>".$row_sql[1]."</td><td>".$row_sql[3]."</td><td>".$row_sql[6]."</td><td>".$row_sql[4]."</td>
                      <td><a href=\"management.php?pag=ver_sol&sol=".$row_sql[0]."\" class=\"btn btn-xs btn-success\">Ver</a>
                      <a href=\"sql.php?sol=".$row_sql[0]."\" class=\"btn btn-xs btn-success\"> Entregar</a></td>
                    </tr>";
            }
            ?>
          </table>
          <a href="management.php" class="btn btn-xs btn-success"> Volver</a>

            <nav class="text-center">
              <ul class="pagination pagination-md">
                <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                <li class="active"><a href="">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="">4</a></li>
                <li><a href="">5</a></li>
                <li><a href=""><span aria-hidden="true">&raquo;</span></a></li>
              </ul>
            </nav>
        </div>
      </div>
  </section>
</div>