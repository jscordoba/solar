<?php
  //session_start();

  if (!isset($_SESSION['id_sesion']))
  {
    ?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Cubicador de Mercancia">
    <meta name="author" content="Jeison Stevens Cordoba">
    
    <link rel="icon" href="../img/favicon.ico">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">

    <script type="text/javascript" src="../bootstrap/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../bootstrap/js/styles.js"></script>

    <?php
    //echo "Sin sesion";
  }

  	include "conn.php";
	$sql=mysqli_query($conn,"SELECT * FROM t_funcionario");

?>
<script type="text/javascript">
/*$(document).ready(function(){

	$("#btn_guardar").click(function(){
		$("#btn_guardar").remove();
		$(".div_btn_guarda").append("<img src='../img/circulo_carga.gif' width='35rem' height='35rem' class='center-block'><p class='text-center'>Cargando...</p>");
	});

});*/
	

	var doc_cont=1;
	var total_docs=1;

	function nuevo_doc(){
		//alert("antes Nuevo Doc "+total_docs);
		doc_cont+=1;
		total_docs+=1;
		$("#mas_docs").append("<div class='form-group'><label for='objeto' class='control-label col-xs-4 col-sm-12 col-md-4'>Documento "+(doc_cont)+"</label><div class='col-xs-7 col-sm-11 col-md-7'><input type='text' id='documento"+doc_cont+"' name='documento"+doc_cont+"' class='form-control doc_add' placeholder='Nuevo Documento "+doc_cont+"' required></div><div class='col-xs-1 col-sm-1 col-md-1'><button class='btn btn-default btn-md btn-success' onclick='quitar_doc(\"documento"+doc_cont+"\")'><span class='glyphicon glyphicon-plus-sign glyphicon-minus-sign'></span></button></div></div>");

		//alert("despues Nuevo Doc "+total_docs);
		$("#total_docs").val(total_docs);
		$("#total_document").val(doc_cont);
	}

	function quitar_doc(id){
		//alert("antes Quitar Doc "+total_docs);
		$("#"+id+"").parent().parent().remove();
		total_docs-=1;

		//alert("despues Quitar Doc "+total_docs);
		$("#total_docs").val(total_docs);
	}
</script>
<?php
	if (!isset($_SESSION['id_sesion'])) {
		?><div class="col-md-8 col-md-offset-2 text-center"><?php
	}else{
		?><div class="col-md-8 col-md-offset-2 text-center vcenter"><?php
	}
?>
	<div class="col-xs-12 col-sm-10 col-md-8 col-md-offset-1 container form-horizontal">
		<h2>Solicitud de Documentos</h2>
		<br>
		<?php
		if (!isset($_SESSION['id_sesion'])) {
			?><form action="sql.php" method="POST"><?php
		}else{
			?><form action="management.php?pag=funct" method="POST"><?php
		}
		?>
			<div class="form-group"  class="col-md-12">
				<label for="nom_solicitante" class="control-label col-sm-12 col-md-4">Solicitante *</label><!-- funcionarios de Ingenieria -->
				
				<div class="col-sm-12 col-md-8">
					<input type="text" id="solicitante" name="solicitante" class="form-control" placeholder="Nombre del solicitante" required>
				</div>
			</div>

			<div class="form-group"  class="col-md-12">
				<label for="email_solicitante" class="control-label col-sm-12 col-md-4">Email Solicitante *</label><!-- funcionarios de Ingenieria -->
				
				<div class="col-sm-12 col-md-8">
					<input type="email" id="email_solicitante" name="email_solicitante" class="form-control" placeholder="email@magnetron.com.co" required>
				</div>
			</div>

			<!--<div class="form-group">
				<label for="contratista" class="control-label col-md-1">Tipo Documento</label>
				<div class="col-md-4">
					<select class="form-control" name="area">
						<option>Seleccione</option>
					</select>
				</div>
			</div>-->

			<div class="form-group">
				<label for="objeto" class="control-label col-sm-12 col-md-4">Tipo de Archivo *</label>
				<div class="col-sm-12 col-md-8">
					<select class="form-control" name="tipo_archivo" required>
						<option default>Seleccione</option>
						<option value="Tecnico">Técnico</option>
						<option value="Comercial">Comercial</option>
					</select>
				</div>
			</div>

			<!-- --------------------------------------------------------------------------- -->
			<div class="form-group">
				<label for="objeto" class="control-label col-xs-4 col-sm-12 col-md-4">Documento Principal *</label>
				<div class="col-xs-7 col-sm-10 col-md-7">
					<input type="text" id="documento" name="documento1" class="form-control" placeholder="Documento principal" required>
				</div>
				<div class="col-xs-1 col-sm-1 col-md-1">
					<button class="btn btn-default btn-md btn-success" onclick="nuevo_doc()"><span class="glyphicon glyphicon-plus-sign glyphicon-plus-sign"></span></button> <!--Agregar campo de texto para un nuevo documento -->
				</div>
			</div>

			<div id="mas_docs">
				<!-- EN ESTE LUGAR SE AGREGAN LOS CAMPOS PARA N DOCUMENTOS -->
			</div>

			<!--
			<div class="form-group">
				<label for="objeto" class="control-label col-sm-12 col-md-4">Documento 2 (Opcional)</label>
				<div class="col-sm-12 col-md-8">
					<input type="text" id="documento" name="documento2" class="form-control" placeholder="Documento opcional">
				</div>
			</div>
			<div class="form-group">
				<label for="objeto" class="control-label col-sm-12 col-md-4">Documento 3 (Opcional)</label>
				<div class="col-sm-12 col-md-8">
					<input type="text" id="documento" name="documento3" class="form-control" placeholder="Documento opcional">
				</div>
			</div>
			-->

			<div class="form-group">
				<label for="objeto" class="control-label col-sm-12 col-md-4">Observaciones (Opcional)</label>
				<div class="col-sm-12 col-md-8">
					<textarea class="form-control" name="observacion">
					</textarea>
				</div>
			</div>
			<!-- --------------------------------------------------------------------------- -->

			<!--<div class="form-group">
				<div class="col-md-6 col-md-offset-2">
					<input type="text" id="documento1" class="form-control" placeholder="Solicitud de nuevo documento">
				</div>
				<button class="btn btn-default btn-md btn-danger"><span class="glyphicon glyphicon-minus-sign glyphicon-minus-sign"></span></button>Quitar campo de texto de un nuevo documento agregado 
			</div>-->

			<div class="form-group">
				<div class="col-md-12 text-right div_btn_guarda">
					<input type="hidden" value="nueva_solicitud" name="opcion">
					<input type="hidden" value="1" name="total_docs" id="total_docs">
					<input type="hidden" value="1" name="total_document" id="total_document">
					<button class="btn btn-success" type="submit" id="btn_guardar" ><span class="glyphicon glyphicon-download-alt"></span> Guardar</button><!-- guardar fecha y hora de solicitud --> 
					<?php
					if (!isset($_SESSION['id_sesion'])) {
					}else{
						?> <a href="management.php" class="btn btn-success"> Volver</a> <?php
					}
					?>
				</div>
			</div>

			<!--<div class="form-group">
				<label for="contratista" class="control-label col-md-1">Área</label>
				<div class="col-md-4">
					<select class="form-control" name="area">
						<option>Seleccione</option>
					</select>
				</div>
			</div>-->
		</form>
	</div>
</div>
<br><br><br><br><br>